import React, { Component } from 'react';
import {
	KeyboardAvoidingView,
	AppRegistry,
	Text,
	TextInput,
	StyleSheet,
	View,
	TouchableHighlight
	} from 'react-native';

export default class MyProject extends Component {
    constructor(props){
	  super(props);
	  this.state = {
		  text: '', 
		  behavior: 'padding'
		  };
	  
  }
  render() {
	var limit = 140;
	var remainder = limit - this.state.text.length;
	var remainderColor = remainder > 10 ? 'purple' : 'red'; 
    return (
      <View style={styles.outerContainer}>
	  <KeyboardAvoidingView behavior={this.state.behavior} style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerTitle}>PUBLIC</Text>
          <TouchableHighlight style={styles.closeButton}>
              <Text>X</Text>
          </TouchableHighlight>
        </View>
		
        <View style={styles.content}>
          <TextInput
			multiline = {true}
			autoCapitalize = 'sentences'
			autoCorrect = {false}
			onChangeText = {(text) => {
				this.setState({text});
			}}
			style = {styles.inputStyle}
			value = {this.state.text}
		/>
        </View>
		
        <View style={styles.footer}>
            <TouchableHighlight style={styles.citeButton}>
              <Text style={styles.citeBtnName}>"</Text>
            </TouchableHighlight>
            <TouchableHighlight style={styles.addButton}>
              <Text style={styles.addBtnName}>+</Text>
            </TouchableHighlight>
			<Text style={[styles.remainder, {color: remainderColor}]}>
				{remainder}
			</Text>
        </View>
		</KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
  },
  container: {
	flex: 1,
  },
  header: {
    height: 35,
    backgroundColor: 'purple',
  },
  headerTitle: {
    textAlign: 'center',
    marginTop: 7,
    color: 'white',
	fontSize: 17,
    fontWeight: 'bold'
  },
  closeButton: {
    position: 'absolute',
    top: 7,
    left: 325,
  },
  content: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputStyle: {
    textAlign: 'center',
    width: 330,
    minHeight: 550,
    fontSize: 16,
	borderBottomColor: 'white'
  },
  footer: {
    height: 42,
  },
  citeButton: {
    position: 'absolute',
    left: 135,
	bottom: 15,
    backgroundColor: 'white',
    borderRadius: 50,
    width: 25,
    height: 25,
    borderColor: 'purple',
    borderWidth: 2,
    borderStyle: 'solid'
  },
  citeBtnName: {
    color: 'purple',
    fontWeight: 'bold',
    fontSize: 21,
	marginBottom: 8,
    position: 'absolute',
    top: 0,
    left: 7,
    bottom: 0,
    right: 0
  },
  addButton: {
    position: 'absolute',
    left: 175,
    bottom: 7,
  },
  addBtnName: {
    textAlign: 'center',
    fontSize: 21,
	marginBottom: 8,
    color: 'purple',
    fontWeight: 'bold'
  },
  remainder: {
    position: 'absolute',
    left: 310,
    top: 7
  }
});
AppRegistry.registerComponent('MyProject', () => MyProject);